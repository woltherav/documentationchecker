/* This file is part of the KDE project
 * SPDX-FileCopyrightText: 2021 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-2.1
 */

package org.kde.documentationChecker;

import org.languagetool.markup.AnnotatedTextBuilder;
import org.languagetool.markup.AnnotatedText;
import java.lang.Object;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class that checks line-by-line through an restructured file and separates out the mark up from the text that
 * can be parsed. transformText() will handle the parsing, and annotatedText() will contain the required text.
 *
 * TODO:
 * - [citations]
 * - |replacements|
 * - text inside tables
 */
public class rstTextBuilder extends Object {

	private AnnotatedTextBuilder builder = new AnnotatedTextBuilder();
	private AnnotatedText annotatedText;

	private boolean commentOrCode = false;
	private boolean tableMode = false;

	// current indentation needs to be tracked to handle block-level syntax.
	private int currentIndentation = -1;

	private Pattern indentationPattern = Pattern.compile("[\\s]+", Pattern.CASE_INSENSITIVE);

	private String directivePattern = "(?<directive>\\.\\.[\\s]+(?<directiveName>[^\\s]+?)\\:\\:)"; // .. image::
	private String labelPattern = "(?<label>^\\.\\.\\s_[^\\s]+?\\:)";
	private String commentPattern = "(?<comment>^\\.\\.\\s)";
	private String fieldPattern ="(?<field>^\\:(?<fieldName>[^\\s]+)\\:[$|\\s])"; // :field:, a type of list.

	private String tableHeaderPattern1 = "^(?<tableHeader1>\\+[\\+\\-]+\\+)$";
	private String tableHeaderPattern2 = "^(?<tableHeader2>[\\=]+\\s[\\+\\s]+\\=)$";

	// section header patterns are also used for direct quotes.
	private String sectionHeaderPattern = "^(?<sectionOrQuote>[\\!\\\"\\#\\$\\%\\&\\'\\(\\)\\*\\+\\,\\-\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\\\\\]\\^\\_\\`\\{\\|\\}\\~]{2,})";

	// handles unordered lists, #., 1. a. 1), (1) and roman numerals.
	private String listPatternUnordered = "(^[\\*\\+\\-\\•\\‣\\⁃]\\s+)";
	private String listPatternOrdered = "(^\\(?[\\#\\d\\w][\\.\\)]\\s+)";
	private String listPatternRoman = "(^\\(?[IVDCMivdcm]*?[\\.\\)]\\s+)";
	private Pattern blockPattern = Pattern.compile(listPatternOrdered
			+ "|" + listPatternUnordered
			+ "|" + listPatternRoman
			+ "|" + fieldPattern
			+ "|" + directivePattern
			+ "|" + labelPattern
			+ "|" + commentPattern
			+ "|" + tableHeaderPattern1
			+ "|" + tableHeaderPattern2
			+ "|" + sectionHeaderPattern
			);

	private String boldPattern = "(?<bStart>\\*+?)(?<bText>[^\\s][^\\*]+?)(?<bEnd>\\*+?)"; // **bold** || *italics* 3 groups
	private String literalPattern = "(?<literal>\\`{2}[^\\s][^\\`]+?\\`{2})"; // ``literal`` 1 group
	private String SimpleReferenceNameChars = "\\w_\\.:\\+\\-"; // [reference] 
	private String rolePattern = "(?<roleStart>\\:[^\\:]*?\\:\\`|\\`)(?<roleText>[^`\\<]+?)(?<roleLink>\\s?\\<[^`\\>]*?\\>)?(?<roleEnd>\\`_?)"; // :role:`text with spaces and stuff, y'now?`, 3 groups.
	
	private Pattern inlineMarkupPattern = Pattern.compile(boldPattern + "|" + literalPattern + "|" + rolePattern, Pattern.UNICODE_CASE);
	
	/**
	 * After running transformText(), this will return the annotated text object.
	 * @return the annotated text object that can be used by language tool.
	 */
	public AnnotatedText annotatedText() {
		return annotatedText;
	}
	
	/**
	 * transformText will add the file data into the annotated text builder,
	 * and at the end, build the annotated text. The annotated text is in annotatedText();
	 * @param file the file containing the text marked up with rst.
	 */
	public void transformText(File file) {
		
		Scanner textReader;
		try {
			textReader = new Scanner(file);
		
			while (textReader.hasNextLine()) {
				String line = textReader.nextLine();
				
				builder.addText("\n");

				if (commentOrCode || tableMode) {
					
					Matcher indentMatch = indentationPattern.matcher(line);
					
					int indentationForLine = -1;
					if (indentMatch.find()) {
						indentationForLine = indentMatch.end();
					}
					if (currentIndentation < 0 && !line.isBlank()) {
						currentIndentation = indentationForLine;
					}
					
					if (indentationForLine < currentIndentation && !line.isBlank()) {
						commentOrCode = false;
					} else {
						builder.addMarkup(line);
						continue;
					}
				}

				if (line.isBlank() || line.isEmpty()) {
					if (tableMode) {
						tableMode = false;
					}
					builder.addMarkup(line);
					continue;

				}

				if (Character.isWhitespace(line.charAt(0))) {
					Matcher indentMatch = indentationPattern.matcher(line);
					if (indentMatch.find()) {
						builder.addMarkup(line.substring(0, indentMatch.end()));
					}
					handleBlocks(line.substring(indentMatch.end()));
					continue;
				}
				
				handleBlocks(line);
				
			}
			
			textReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("Can't read file");
			e.printStackTrace();
		}
		
		annotatedText = builder.build();
	}
	
	/**
	 * Tries to handle block-level mark up that isn't
	 * already handled in the main function.
	 * @param line the line to handle.
	 */
	void handleBlocks(String line) {
		int pos = 0;
		
		Matcher markup = blockPattern.matcher(line);
		
		while (markup.find()) {
			if (markup.start()>pos) {
				handleInlineMarkup(line.substring(pos, markup.start()));
			}
			System.out.println("block pattern: "+markup.group());
			if (markup.group("directive") != null) {
				//System.out.println("\n directive?: "+markup.group("directive"));
				if (markup.group("directiveName").contains("code") ||
						markup.group("directiveName").contains("toctree")) {
					
					builder.addMarkup(line);
					currentIndentation = -1;
					commentOrCode = true;
					return;
				} else {
					//System.out.println("\n directive: "+markup.group("directiveName"));
					builder.addMarkup(markup.group("directive"));
				}
			} else if (markup.group("label") != null) {

				builder.addMarkup(line);
				return;

			}  else if (markup.group("tableHeader1") != null ||
					markup.group("tableHeader2") != null) {
				//Tables get completely stripped right now...
				//System.out.println("\n table: "+line);
				builder.addMarkup(line);
				currentIndentation = -1;
				tableMode = true;
				return;

			} else if (markup.group("comment") != null) {

				builder.addMarkup(line);
				//System.out.println("\n comment: "+line);
				currentIndentation = -1;
				commentOrCode = true;
				return;

			} else if (markup.group("field") != null) {
				if (markup.group("fieldName").contains("description")
						|| markup.group("fieldName").contains("alt")
						|| markup.group("fieldName").contains("caption")) {
					builder.addMarkup(line.substring(markup.start(), markup.end()), "\n");
				} else {
					builder.addMarkup(line);
					return;
				}
			}
			
			builder.addMarkup(line.substring(markup.start(), markup.end()), "\n");
			
			pos = markup.end();
		}
		if (pos < line.length()-1) {
			handleInlineMarkup(line.substring(pos));
		}
		
		return;
	}
	
	/**
	 * Handles inline markup.
	 * @param line the segment to handle.
	 */
	void handleInlineMarkup (String line) {
		
		Matcher inlineMarkup = inlineMarkupPattern.matcher(line);
		
		//System.out.println("\n"+line+"\n");
		
		int pos = 0;
		while (inlineMarkup.find()) {
			
			
			if (inlineMarkup.start()>pos) {
				//System.out.println("line start -- "+line.substring(pos, inlineMarkup.start()));
				handleReadingMarks(line.substring(pos, inlineMarkup.start()));
			}
			if (inlineMarkup.group("bStart") != null) {
				builder.addMarkup(line.substring(inlineMarkup.start("bStart"), inlineMarkup.end("bStart")));
				handleReadingMarks(line.substring(inlineMarkup.start("bText"), inlineMarkup.end("bText")));
				builder.addMarkup(line.substring(inlineMarkup.start("bEnd"), inlineMarkup.end("bEnd")));
				//System.out.println("btext -- "+line.substring(inlineMarkup.start("bText"), inlineMarkup.end("bText")));
			}
			
			if (inlineMarkup.group("literal") != null) {
				builder.addMarkup(line.substring(inlineMarkup.start("literal"), inlineMarkup.end("literal")), "replaced-literal");
			}
			
			
			if (inlineMarkup.group("roleStart") != null) {
				
				//System.out.println("\n role: "+inlineMarkup.group());
				builder.addMarkup(line.substring(inlineMarkup.start("roleStart"), inlineMarkup.end("roleStart")));
				
				String roleText = line.substring(inlineMarkup.start("roleText"), inlineMarkup.end("roleText"));
				//System.out.println("\n roletext: "+roleText);
				if (inlineMarkup.group("roleLink") != null) {
					handleReadingMarks(roleText);
					builder.addMarkup(line.substring(inlineMarkup.start("roleLink"), inlineMarkup.end("roleLink")));
				} else if (roleText.contains("_") || inlineMarkup.group("roleStart").contains("file") ){
					builder.addMarkup(roleText, "replaced-link");
				} else {
					handleReadingMarks(roleText);
				}
				builder.addMarkup(line.substring(inlineMarkup.start("roleEnd"), inlineMarkup.end("roleEnd")));
				//System.out.println("role text -- "+line.substring(inlineMarkup.start("roleText"), inlineMarkup.end("roleText")));
			}
			
			pos = inlineMarkup.end();
			
		}
		if (pos < line.length()) {
			handleReadingMarks(line.substring(pos));
			//System.out.println("end text -- "+line.substring(pos));
		}
		
		return;
	}
	
	/**
	 * handles reading marks like arrows,
	 * which need to be replaced separately.
	 * @param line the subsection to check for marks.
	 */
	void handleReadingMarks(String line) {
		int pos = 0;

		String arrow = "(?<arrow>\\s\\-\\-\\>\\s)";
		
		Pattern marks = Pattern.compile(arrow);
		
		Matcher markup = marks.matcher(line);
		
		while (markup.find()) {
			//System.out.println(markup.groupCount() + markup.group());
			if (markup.start()>pos) {
				builder.addText(line.substring(pos, markup.start()));
			}
			if (markup.group("arrow") != null) {
				builder.addMarkup(line.substring(markup.start("arrow"), markup.end("arrow")), " → ");
			}
			
			pos = markup.end();
		}
		if (pos < line.length()) {
			builder.addText(line.substring(pos));
		}
		
		return;
	}

}
