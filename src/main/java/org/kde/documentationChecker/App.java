/* This file is part of the KDE project
 * SPDX-FileCopyrightText: 2021 Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-2.1
 */

package org.kde.documentationChecker;
import org.kde.documentationChecker.rstTextBuilder;

import org.languagetool.JLanguageTool;
import org.languagetool.Language;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.rules.CategoryId;
import org.languagetool.rules.Rule;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.spelling.SpellingCheckRule;
import org.languagetool.tools.ContextTools;


import org.json.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FileReader;

/**
 * This checker creates an annotated text of the rst file, where all the markup is marked.
 * Then said annotated text is fed to languageTool. The results are put into a json file
 * that is compatible with code-climate/gitlab code quality.
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	if (args[0] == null || args[0].trim().isEmpty()) {
            System.out.println("You need to specify a path! First path is rst file, second the word config json, and third the output json file to append to.");
            return;
        }
    	Language lang = new AmericanEnglish();
    	
    	JLanguageTool langTool = new JLanguageTool(lang);
    	
    	File fileToRead = new File(args[0].toString());

		FileReader configFile;
		JSONObject ruleSeverity = new JSONObject();
		JSONObject categorySeverity = new JSONObject();
		boolean debugOutputEnabled = false;
		List<String> listOfPhrases = new ArrayList<String>();

    	if (!args[1].isEmpty()) {

			try {
				configFile = new FileReader(args[1].toString());
				JSONObject config = new JSONObject(new JSONTokener(configFile));

				if (config.has("categories")) {
					JSONObject categories = config.getJSONObject("categories");
					for (String key: categories.keySet()) {
						JSONObject category = categories.getJSONObject(key);
						if (category.getBoolean("enabled")) {
							langTool.enableRuleCategory(new CategoryId(key));
						} else {
							langTool.disableCategory(new CategoryId(key));
						}
						categorySeverity.put(key, category.getString("severity"));
					}
				}

				if (config.has("rules")) {
					JSONObject rules = config.getJSONObject("rules");
					for (String key: rules.keySet()) {
						JSONObject rule = rules.getJSONObject(key);
						if (rule.getBoolean("enabled")) {
							langTool.enableRule(key);
						} else {
							langTool.disableRule(key);
						}
						ruleSeverity.put(key, rule.getString("severity"));
					}
				}

				if (config.has("debug-output-enabled")) {
					debugOutputEnabled = config.getBoolean("debug-output-enabled");
				}

				if (config.has("accepted-phrases")) {
					JSONArray phrases = config.getJSONArray("accepted-phrases");
					for (int i = 0; i<phrases.length(); i++) {
						listOfPhrases.add(phrases.getString(i));
					}
				}

			} catch (FileNotFoundException e) {
				System.out.println("Can't read file:"+args[1].toString());
				e.printStackTrace();
			}
		}

		for (Rule rule : langTool.getAllActiveRules()) {
			if (rule instanceof SpellingCheckRule) {
				((SpellingCheckRule) rule).acceptPhrases(listOfPhrases);
			}
		}

		rstTextBuilder rst = new rstTextBuilder();
    	rst.transformText(fileToRead);

    	FileWriter output;
    	JSONArray jsonFile = new JSONArray();
    	String filepath = fileToRead.getAbsolutePath().replace(".rst", ".json");

    	if (!args[2].isEmpty()) {
    		if (Files.exists(Paths.get(args[2].toString()))) {
    			try {
    				FileReader outputAppend = new FileReader(args[2].toString());
    				jsonFile = new JSONArray(new JSONTokener(outputAppend));
    				try {
    					outputAppend.close();
    				} catch (IOException e) {
    					System.out.println("Can't close file: "+ args[2]);
    					e.printStackTrace();
    				}
    				filepath = args[2].toString();

    			} catch (FileNotFoundException e) {
    				System.out.println("Can't read file: "+ args[2]);
    				e.printStackTrace();
    			}
    		} else {
    			filepath = args[2].toString();
    		}
    	} else {
    		System.out.println("Not appending, making new file: "+filepath);
    	}
    	
    	List<RuleMatch> matches;
		try {
			matches = langTool.check(rst.annotatedText());
			output = new FileWriter(filepath);
			
			if (debugOutputEnabled) {
				FileWriter debug = new FileWriter(fileToRead.getAbsolutePath().replace(".rst", "_debug_output.txt"));
				debug.append(rst.annotatedText().getPlainText());
				debug.close();
			}
			ContextTools context = new ContextTools();
			// keep wondering what would be the best error marker...
			context.setErrorMarker("~", "~");
			context.setContextSize(5);
			context.setEscapeHtml(false);
			
			for (RuleMatch match : matches) {
				JSONObject matchJson = new JSONObject();
				
				String affected = context.getContext(match.getFromPos(), match.getToPos(), rst.annotatedText().getTextWithMarkup());
				long lineNumber = rst.annotatedText().getTextWithMarkup().substring(0, match.getFromPos()).lines().count() - 1;
				long lineEnd = affected.lines().count()-1;
				
				String matchDescription = affected + " — " + match.getMessage().replaceAll("\\<\\/?suggestion\\>", "`");
				String matchContent = String.format("Type: %s, Category: %s, Position: %s-%s \n\n%s \nProblem: %s"
						, match.getType(), match.getRule().getCategory().getId().toString(), match.getFromPos()
						, match.getToPos(), match.getSentence().getText()
						, match.getMessage().replaceAll("\\<\\/?suggestion\\>", "`")); 
				
				if (!match.getSuggestedReplacements().isEmpty()) {
					matchContent += String.format("\nSuggestion: %s.", match.getSuggestedReplacements());
				}
				
				String url = match.getUrl() != null? match.getUrl().toString(): "";
				if (url.isBlank()) { url = match.getRule().getUrl() != null? match.getRule().getUrl().toString(): "";}
				if (!url.isBlank()) {
					matchContent += String.format("\n[Explanation](%s).", url);
				}
				
				matchJson.put("type", "issue");
				matchJson.put("fingerprint", String.format("%s:%s:%s", fileToRead.getAbsolutePath(), match.getFromPos(), match.getToPos()));
				matchJson.put("description", matchDescription);
				matchJson.put("content", matchContent);
				matchJson.put("check_name", match.getRule().getFullId());

				String severity = "info";

				String id = match.getRule().getCategory().getId().toString();
				if (categorySeverity.has(id)) {
					severity = categorySeverity.getString(id);
				}
				
				id = match.getRule().getFullId().toString();
				if (ruleSeverity.has(id)) {
					severity = ruleSeverity.getString(id);
				}

				matchJson.put("severity", severity);
				matchJson.put("categories", new JSONArray("[\"Style\"]"));
				
	    	    JSONObject beginJson = new JSONObject();
	    	    beginJson.put("line", lineNumber);
	    	    //beginJson.put("column", match.getColumn());
	    	     
	    	    JSONObject endJson = new JSONObject();
	    	    endJson.put("line", lineNumber+lineEnd);
	    	    //endJson.put("column", match.getEndColumn());
	    	     
	    	    JSONObject positionJson = new JSONObject();
	    	    positionJson.put("begin", beginJson);
	    	    positionJson.put("end", endJson);
	    	    
	    	    JSONObject linesJson = new JSONObject();
	    	    linesJson.put("begin", lineNumber);
	    	     
	    	    JSONObject locationJson = new JSONObject();
	    	    locationJson.put("path", fileToRead.getAbsolutePath());
	    	    locationJson.put("position", positionJson);
	    	    locationJson.put("lines", linesJson);
	    	    matchJson.put("location", locationJson);
	    	    jsonFile.put(matchJson);
			}
			System.out.println("Matches found: "+jsonFile.length());
			output.append(jsonFile.toString(2));
			output.close();
		} catch (IOException e) {
			System.out.println("Can't read file");
			e.printStackTrace();
		}
    	
    	
    	
    }
}
